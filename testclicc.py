#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 28 15:00:56 2020

@author: cmi

"""

import clicc

'''
main
'''
URL_orange = \
  'https://cloud.orange-business.com/nos-tarifs/elastic-cloud-server/'
URL_wekeo = 'https://wekeo.eu/web/guest/price-list'
URL_ovh = 'https://www.ovhcloud.com/fr/public-cloud/prices/'
URL_aws = 'https://aws.amazon.com/fr/ec2/pricing/on-demand/'

# original file is password-locked, unlocked with https://smallpdf.com/fr/unlock-pdf
file_Tsystems = 'tests/open-telekom-cloud-service-description-unlocked.pdf'

orange_file = 'tests/orange.yaml'
wekeo_file = 'tests/wekeo.yaml'
ovh_file = 'tests/ovh.yaml'
tsystems_file = 'tests/tsystems.yaml'
fitness_file = 'tests/fitness.yaml'

Online = False

Fit_VMs = clicc.simulate_fitness()
Fit_VMs.dump_to_yaml(fitness_file)
exit(0)

if Online:

    Orange_VMs = clicc.parse_orange(URL_orange)
    Orange_VMs.dump_to_yaml(orange_file)

    Wekeo_VMs = clicc.parse_wekeo(URL_wekeo)
    Wekeo_VMs.dump_to_yaml(wekeo_file)

    Ovh_VMs = clicc.parse_ovh(URL_ovh)
    Ovh_VMs.dump_to_yaml(ovh_file)

    Tsystems_VMs = clicc.parse_Tsystems(file_Tsystems)
    Tsystems_VMs.dump_to_yaml(tsystems_file)

else:

    Orange_VMs = clicc.load_from_yaml(orange_file)
    Wekeo_VMs = clicc.load_from_yaml(wekeo_file)
    Ovh_VMs = clicc.load_from_yaml(ovh_file)
    Tsystems_VMs = clicc.load_from_yaml(tsystems_file)

labels = []
Fit_Prices = []
Orange_Prices = []
ovh_prices = []
wekeo_prices = []
tsystems_prices = []

for CPU in [1, 2, 4, 8, 16, 32]:
    RAM = 8*CPU
    label = str(CPU) + ' vCPUs, ' + str(RAM) + ' GB RAM'
    labels.append(label)

    Exists, MyVM = Fit_VMs.find_cheapest(CPU, RAM)
    if Exists:
        Fit_Prices.append(MyVM.monthly)
    else:
        Fit_Prices.append(0)

    Exists, MyVM = Orange_VMs.find_cheapest(CPU, RAM)
    if Exists:
        Orange_Prices.append(MyVM.monthly)
    else:
        Orange_Prices.append(0)

    Exists, MyVM = Wekeo_VMs.find_cheapest(CPU, RAM)
    if Exists:
        wekeo_prices.append(MyVM.monthly)
    else:
        wekeo_prices.append(0)

    Exists, MyVM = Ovh_VMs.find_cheapest(CPU, RAM)
    if Exists:
        ovh_prices.append(MyVM.monthly)
    else:
        ovh_prices.append(0)

    Exists, MyVM = Tsystems_VMs.find_cheapest(CPU, RAM)
    if Exists:
        tsystems_prices.append(MyVM.monthly)
    else:
        tsystems_prices.append(0)


clicc.plot('Monthly prices per CSP', labels,
           ['FITNESS', 'ORANGE', 'OVH', 'TSYSTEMS', 'WEKEO'],
           Fit_Prices, Orange_Prices, ovh_prices, tsystems_prices,
           wekeo_prices)

labels = []
Orange_Prices = []

for one_VM in Orange_VMs:
    labels.append(one_VM.name + " | CPU " + str(one_VM.CPU) + "| RAM "
                  + str(one_VM.RAM))
    Orange_Prices.append(one_VM.monthly)

clicc.plot('Montly prices ORANGE', labels, ['ORANGE'], Orange_Prices)

labels = []
ovh_prices = []

for one_VM in Ovh_VMs:
    labels.append(one_VM.name)
    ovh_prices.append(one_VM.monthly)

clicc.plot('Montly prices OVH', labels, ['OVH'], ovh_prices)

labels = []
tsystems_prices = []

for one_VM in Tsystems_VMs:
    labels.append(one_VM.name)
    tsystems_prices.append(one_VM.monthly)

clicc.plot('Montly prices TSYSTEMS', labels, ['TSYSTEMS'], tsystems_prices)
