#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 07 08:37:12 2021

@author: cmi

"""

import clicc
import argparse

'''
main
'''
# set-up the CSP variables
URL_orange = \
  'https://cloud.orange-business.com/nos-tarifs/elastic-cloud-server/'
URL_wekeo = 'https://wekeo.eu/web/guest/price-list'
URL_ovh = 'https://www.ovhcloud.com/fr/public-cloud/prices/'
URL_aws = 'https://aws.amazon.com/fr/ec2/pricing/on-demand/'

# original file is password-locked, unlocked with https://smallpdf.com/fr/unlock-pdf
file_Tsystems = 'tests/open-telekom-cloud-service-description-unlocked.pdf'

orange_file = 'tests/orange.yaml'
wekeo_file = 'tests/wekeo.yaml'
ovh_file = 'tests/ovh.yaml'
tsystems_file = 'tests/tsystems.yaml'
fitness_file = 'tests/fitness.yaml'

# create the CSP instances
Orange_CSP = clicc.CSP("Orange", URL_orange, orange_file, clicc.load_from_yaml(orange_file))
Wekeo_CSP = clicc.CSP("Wekeo", URL_wekeo, wekeo_file, clicc.load_from_yaml(wekeo_file))
Ovh_CSP = clicc.CSP("OVH", URL_ovh, ovh_file, clicc.load_from_yaml(ovh_file))
Tsystems_CSP = clicc.CSP("Tsystems", file_Tsystems, tsystems_file, clicc.load_from_yaml(tsystems_file))
Fitness_CSP = clicc.CSP("Fitness", "NA", fitness_file, clicc.load_from_yaml(fitness_file))

All_CSPs = [Orange_CSP, Wekeo_CSP, Ovh_CSP, Tsystems_CSP, Fitness_CSP]

Online = False

# Create the parser
my_parser = argparse.ArgumentParser(prog='CLICLICC',
                                    description='request comparisons from CLICC')


# little twist to allow comma-separated list of values
def csv_list(string):
    return string.split(',')


# handle the arguments
my_parser.add_argument('--vcpu', action='store', type=csv_list, required=True)
my_parser.add_argument('--ram', action='store', type=csv_list, required=True)

args = my_parser.parse_args()

if len(args.vcpu) != len(args.ram):
    print("Please give me the same nb of vcpu and ram values")
    exit(1)

# seek VMs
labels = []
prices = dict()
for CSP in All_CSPs:
    prices[CSP.name] = []
i = 0

for CPU in args.vcpu:
    CPU = int(CPU)
    RAM = int(args.ram[i])
    i += 1

    label = str(CPU) + ' vCPUs, ' + str(RAM) + ' GB RAM'
    labels.append(label)

    for CSP in All_CSPs:
        Exists, MyVM = CSP.VMs.find_cheapest(CPU, RAM)
        if Exists:
            (prices[CSP.name]).append(MyVM.monthly)
        else:
            (prices[CSP.name]).append(0)

clicc.plot_prices('Monthly prices per CSP', labels, prices)
