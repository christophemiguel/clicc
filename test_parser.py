#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import clicc
import os
import filecmp
'''
main
'''

# get the input params through env vars
parser_under_test = os.environ['PARSER']
URL = os.environ['URL_'+ parser_under_test]
ref_file = os.environ['FILE_'+ parser_under_test]

print('URL = ' + URL)
print('ref_file = ' + ref_file)
test_file = "tests/test.yaml"

VMs = []

# run the proper parser
if parser_under_test == "OVH":
    VMs = clicc.parse_ovh(URL)
elif parser_under_test == "ORANGE":
    VMs = clicc.parse_orange(URL)
elif parser_under_test == "WEKEO":
    VMs = clicc.parse_wekeo(URL)
elif parser_under_test == "TSYSTEMS":
    VMs = clicc.parse_Tsystems(URL)
elif parser_under_test == "FITNESS":
    VMs = clicc.simulate_fitness()
else:
    print('PARSER ' + parser_under_test + 'unknown')
    exit(1)

VMs.dump_to_yaml(test_file)

if filecmp.cmp(ref_file, test_file, shallow=False):
    print("test successful")
    exit(0)
else:
    print("test failed")
    exit(1)
