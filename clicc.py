#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 28 15:00:21 2020

@author: cmi

CLICC = CLoud Infrastructure Cost Calculation
"""

import requests
from selenium import webdriver
from bs4 import BeautifulSoup
import tabula
import matplotlib.pyplot as plt
import numpy as np
import yaml
import pandas as pd
import time


class VM(object):
    ''' standardized Virtual Machine '''

    def __init__(self, CSP, name, kind, CPU, RAM, disk, hourly_rate,
                 monthly_rate):
        self.CSP = CSP
        self.name = name
        self.kind = kind
        self.CPU = int(CPU)
        self.RAM = int(RAM)
        self.disk = disk
        self.hourly = float(hourly_rate)
        self.monthly = float(monthly_rate)
        # self.yearly_rate=yearly_rate

    def display(self):
        print(f'CSP={self.CSP} Name={self.name} type={self.kind}'
              f'CPU={self.CPU} RAM={self.RAM} disk={self.disk} '
              f'hourly={self.hourly} monthly={self.monthly}')


class ListOfVMs(list):
    '''List of VMs'''

    def find_1st(self, CPU, RAM):
        Found = False
        iterator = 0
        # stop iterating as soon as one match found
        while (not Found) & (iterator < len(self)):
            if (self[iterator]).CPU == CPU and (self[iterator]).RAM == RAM:
                Found = True
                return Found, self[iterator]
            iterator += 1

        return Found, self[iterator]

    def find_cheapest(self, CPU, RAM):
        Found = False
        iterator = 0
        FoundVM = self[iterator]
        # iterate along the whole list
        while (iterator < len(self)):
            if (self[iterator]).CPU == CPU and \
               np.isclose((self[iterator]).RAM, RAM, 0.1):
                # if none was found before
                if not Found:
                    FoundVM = self[iterator]
                    Found = True
                # if one was found before, check if cheaper
                else:
                    if (self[iterator]).monthly < FoundVM.monthly:
                        FoundVM = self[iterator]
            iterator += 1

        return Found, FoundVM

    def dump_to_yaml(self, file_name):
        with open(file_name, 'w') as f:
            data = yaml.dump(self, f)

class CSP(object):
    ''' standardized Cloud Service Provider '''

    def __init__(self, name, URL, file, VMs):
        self.name = name
        self.URL = URL
        self.file = file
        self.VMs = ListOfVMs(VMs)

    def display(self):
        print(f'Name={self.name} URL={self.URL} file={self.URL}')
        for VM in self.VMs:
            VM.display()


def load_from_yaml(file_name):
    ''' create a list of VMs from an offline yaml file'''

    with open(file_name) as f:
        VMs = list(yaml.load_all(f, Loader=yaml.FullLoader))

    return VMs[0]


def parse_Tsystems(filename):
    ''' read the Tsystems prices PDF '''

    Tsystems_VMs = ListOfVMs()
    # pages 52 to 56 are our target
    df = tabula.read_pdf(filename, pages='52-56', multiple_tables=True)

    # iterate over dataframes per page
    for my_df in df:

        # get the number of lines of the dataframe
        nb_lines = my_df.shape[0]

        # 6 lines per VM starting from index 4
        for i in range(4, nb_lines, 6):
            name = my_df.loc[i+2][0]
            vCPU = my_df.loc[i][1]
            RAM = my_df.loc[i][2]

            # using the 1st row prices for open linux distrib
            hourly = my_df.loc[i][4]
            hourly = hourly.replace('.', '')
            hourly = hourly.replace(',', '.')
            monthly = my_df.loc[i][5]
            monthly = monthly.replace('.', '')
            monthly = monthly.replace(',', '.')
            kind = 'general'

            myVM = VM('Tsystems', name, kind, vCPU, RAM,
                      'unknown', float(hourly), float(monthly))
            Tsystems_VMs.append(myVM)
    return Tsystems_VMs


def parse_orange(URL, filename='orange.txt'):
    ''' scrap the orange pricing webpage provided as URL '''
    
    # use BeautifulSoup to parse webpage
    page = requests.get(URL, verify=False)
    soup = BeautifulSoup(page.content, 'html.parser')

    # find the prices table in the page
    table = soup.find('table', attrs={'class': 'alignleft'})
    table_rows = table.find_all('tr')

    f = open(filename, 'w')

    it = 0
    row1 = ()
    row = ()
    OrangeVMs = ListOfVMs()
    # for each row
    for tr in table_rows:
        td = tr.find_all('td')

        # keep the 1st row as headers
        if it == 0:
            row1 = [tr.text.replace(u'\xa0', u' ') for tr in td]
        else:
            row = [tr.text.replace(u'\xa0', u' ') for tr in td]
            # 1st row for keys,other rows for values
            dicoVM = dict(zip(row1, row))
            f.write(str(dicoVM)+'\n')

            # remove the space and euro sign for prices
            hourly = ((dicoVM['PAYG\n– Hourly Price'].strip())[:-2])
            hourly = hourly.replace(',', '.')
            monthly = ((dicoVM['PAYG\n– Monthly Price [6]'].strip())[:-2])
            monthly = monthly.replace(',', '.')

            # kind of VM depending on Orange descriptions
            kind = 'general'

            # map the values into a VM instance
            myVM = VM('Orange', dicoVM['Product'], kind, dicoVM['vCPU'],
                      dicoVM['RAM\n(GB)'], dicoVM['Data\ndisk'],
                      float(hourly), float(monthly))

            # add it to the list of VMs to be returned
            OrangeVMs.append(myVM)

        it += 1
    f.close()

    return OrangeVMs


def parse_wekeo(URL, filename='wekeo.txt'):
    ''' scrap the wekeo pricing webpage provided as URL '''

    # use BeautifulSoup to parse webpage
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, 'html.parser')

    # find the pricing-details structure in the page
    results = soup.find_all("div", class_="pricing-details")
    prices = soup.find_all("div", class_="pd-price")

    f = open(filename, 'w')

    # loop over results
    for result in results:
        # find all columns headers as VM flavor names
        colonnes = result.find_all("th")
        nb_colonnes = len(colonnes)

        # find all data cells
        data = result.find_all("td")
        # find keys cells
        keys = result.find_all("td", class_="pd-feat")

        WekeoVMs = ListOfVMs()
        # iterate per column/flavor
        for i in range(1, nb_colonnes):
            dicoVM = dict()
            dicoVM["Name"] = colonnes[i].getText()

            it = 0
            for val in keys:
                indice = (it*nb_colonnes)+i
                if val.getText().isprintable():
                    dicoVM[val.getText()] = data[indice].getText()
                it += 1

            # prices data to be reprocessed
            if i <= len(prices):
                whole = prices[i-1].getText()
                dicoVM["monthly"] = (whole.split()[0]).replace(',', '')
                dicoVM["yearly"] = whole.split()[3]
            else:
                dicoVM["monthly"] = 0
                dicoVM["yearly"] = 0

            # RAM to be reprocessed
            dicoVM['RAM'] = (dicoVM['RAM'])[:-3]

            # kind of VM depending on Orange descriptions
            kind = 'general'

            myVM = VM('Wekeo', dicoVM['Name'], kind, dicoVM['vCPUs'],
                      dicoVM['RAM'], dicoVM['File storage'],
                      0, dicoVM['monthly'])
            WekeoVMs.append(myVM)

            # print VMs
            f.write(str(dicoVM))
            f.write('\n')
    f.close()

    return WekeoVMs


def parse_ovh(URL, filename='ovh.txt'):
    ''' scrap the OVH pricing webpage provided as URL '''

    # use BeautifulSoup to parse webpage
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, 'html.parser')

    # find the pricing-details structure in the page
    tables = soup.find_all("table", class_="responsive-enabled")

    OvhVMs = ListOfVMs()

    # loop over tables
    for table in tables[0:3]:

        # find all rows
        rows = table.find_all("tr")

        for row in rows:
            # get prices dictionnary
            raw_prices = row.get('data-price')

            # get Linux monthly and hourly costs from this dict
            if raw_prices is not None:
                prices = eval(raw_prices)
                if 'FRA' in prices:
                    prices_for_fr = prices['FRA']
                elif 'all' in prices:
                    prices_for_fr = prices['all']
                hourly = prices_for_fr['linux.hourly'].split()[0]
                hourly = hourly.replace(',', '.')
                monthly = prices_for_fr['linux.monthly'].split()[0]
                monthly = monthly.replace(',', '.')

                # get data cells and map them to VM attributes
                data = row.find_all("td")
                name = data[0].getText()
                kind = 'general'
                if name.startswith('c'):
                    kind = 'CPU'
                elif name.startswith('r'):
                    kind = 'RAM'
                RAM = (data[1].getText())[:-3]
                vCPU = data[2].getText()
                storage = data[3].getText()

                # create the VM instance
                myVM = VM('OVH', name, kind, vCPU,
                          RAM, storage,
                          hourly, monthly)
                OvhVMs.append(myVM)

    return OvhVMs


def parse_aws(URL, filename='aws.txt'):
    ''' scrap the AWS pricing webpage provided as URL 
    /!\ UNDER CONSTRUCTION'''
    
    # use BeautifulSoup to parse webpage
    driver = webdriver.PhantomJS()
    driver.get(URL)
    time.sleep(10)
    #p_element = driver.find_element_by_id(id_='data-plc-offer-id')
    results = driver.find_element_by_class_name('js-active')
    
    print(results)

    # find the pricing-details structure in the page
    tables = results.find_element_by_class_name('data-plc-offer-id')
    f = open(filename, 'w')
    f.write(str(r))
    f.write('\n')
    f.close()
    
    AWS_VMs = ListOfVMs()

    return AWS_VMs


def simulate_fitness():
    ''' generate FITNESS costs '''

# 1QP =21€/mois
#    RAM=21GB
#    CPU=4vCPU
#    Stockage=225GB
# couts fixes = 174€/mois

    FIX_COST = 174
    QP_COST = 21
    FitnessVMs = ListOfVMs()

    # iterate over nb of vCPU
    for vCPU in [1, 2, 4, 8, 16, 32]:
        QPCPU = (vCPU//4)
        QPstorage = 1
        if QPCPU == 0:
            QPCPU = 1

        # iterate over RAM/CPU rate
        for coeff in [2, 4, 8]:
            RAM = vCPU*coeff
            QPRAM = (RAM//21)
            if QPRAM == 0:
                QPRAM = 1
            monthly = FIX_COST + QP_COST*(QPCPU+QPRAM+QPstorage)

            # kind of VM depending on Orange descriptions
            kind = 'general'
            myVM = VM('FITNESS', 'F'+str(vCPU)+'-'+str(RAM), kind, vCPU, RAM,
                      225, 0, monthly)
            FitnessVMs.append(myVM)

    return FitnessVMs


def plot_prices(title, labels, inprices):
    ''' print a bar graph with the input prices '''

    prices = list(inprices.values())
    CSPs = list(inprices.keys())

    x = np.arange(len(labels))  # the label locations
    width = 0.15  # the width of the bars

    fig, ax = plt.subplots()

    # calculate the relative position of the bars
    position = []
    rect = []
    # case of odd nb of prices lists
    if len(prices) % 2 == 1:
        for i in range(-len(prices)//2+1, len(prices)//2+1):
            position.append(i*width)

    # case of even nb of prices lists
    else:
        for i in range(-len(prices)//2, len(prices)//2+1):
            position.append(i*width/len(prices))
        position.remove(0)

    # iterate over the lists of prices
    for i in range(0, len(prices)):
        rect.append(ax.bar(x+position[i], prices[i], width,
                           label=CSPs[i]))

    def autolabel(rects):
        # attach some text labels
        for rect in rects:
            height = rect.get_height()
            plt.text(rect.get_x()+rect.get_width()/2., 1.05*height,
                     '%d' % int(height),
                     ha='center', va='bottom')

    for i in range(0, len(prices)):
        autolabel(rect[i])

    # Add some text for labels, title and custom x-axis tick labels, etc.
    # ax.set_ylabel('€')
    ax.set_title(title)
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    plt.xticks(rotation=90)
    ax.legend()

    fig.tight_layout()

    filename = title + '.png'
    fig.savefig(filename, bbox_inches='tight')